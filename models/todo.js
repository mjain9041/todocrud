// models/todo.js

const mongoose = require('mongoose');

const todoSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true,
  },
  description: {
    type: String,
  },
  completed: {
    type: Boolean,
    default: false,
  },
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User', // Reference to the User model for associating To-Do items with users
  },
});

const TodoModel = mongoose.model('Todo', todoSchema);

module.exports = TodoModel;
