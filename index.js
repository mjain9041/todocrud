// server.js (or app.js)

const express = require('express');
const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');
const bodyParser = require('body-parser');
const dotenv = require('dotenv'); // Import the dotenv package
const TodoModel = require('./models/todo');
const UserModel = require('./models/user');
const authMiddleware = require('./middleware/auth');

// Load environment variables from .env file
dotenv.config();

const app = express();
const port = process.env.PORT || 3000;

app.use(bodyParser.json());
console.log(process.env.DB_URI)
// MongoDB setup
mongoose.connect(process.env.DB_URI, { useNewUrlParser: true, useUnifiedTopology: true });

// User registration
app.post('/register', async (req, res) => {
  // Implement user registration here (e.g., save user details to MongoDB).
  try {
    const { username, password } = req.body;

    if (!username || !password) {
        return res.status(400).json({ error: 'Both username and password are required' });
    }

    // Check if the username is already taken
    const existingUser = await UserModel.findOne({ username });

    if (existingUser) {
      return res.status(400).json({ error: 'Username already exists' });
    }

    // Create a new user
    const newUser = new UserModel({
      username,
      password,
    });

    // Save the user to the database
    await newUser.save();

    res.status(201).json({ message: 'User registered successfully' });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Internal server error' });
  }
});

// User login
app.post('/login', async (req, res) => {
  // Implement user login and JWT generation here.4
  
  try {
    const { username, password } = req.body;

    // Check if username and password are provided
    if (!username || !password) {
        return res.status(400).json({ error: 'Both username and password are required' });
    }

    // Find the user by username in the database
    const user = await UserModel.findOne({ username });

    if (!user) {
      return res.status(401).json({ error: 'Invalid username or password' });
    }

    // Check if the provided password matches the hashed password in the database
    const passwordMatch = await user.comparePassword(password);

    if (!passwordMatch) {
      return res.status(401).json({ error: 'Invalid username or password' });
    }

    // Generate a JWT token for the user
    const token = jwt.sign({ id: user._id }, 'yourSecretKey'); // Replace with your own secret key

    res.json({ token });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Internal server error' });
  }
});

// API routes for To-Do List
app.use(authMiddleware); // Middleware to check JWT for authentication

app.post('/todos', async (req, res) => {
  // Create a new To-Do item and save it to the database.
  try {
    const { title, description } = req.body;

    if (!title) {
        return res.status(400).json({ error: 'The "title" field is required' });
    }

    const user = req.user; // Assuming the authenticated user is stored in req.user

    // Create a new To-Do item and associate it with the authenticated user
    const newTodo = new TodoModel({
      title,
      description,
      user: user._id, // Associate the To-Do item with the user
    });

    const savedTodo = await newTodo.save();
    res.status(201).json(savedTodo);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Internal server error' });
  }
});

app.get('/todos', async (req, res) => {
  // Fetch a list of To-Do items for the authenticated user.
  try {
    const user = req.user; // Assuming the authenticated user is stored in req.user

    // Fetch To-Do items associated with the authenticated user
    const todos = await TodoModel.find({ user: user._id });
    
    res.status(200).json(todos);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Internal server error' });
  }
});

app.put('/todos/:id', async (req, res) => {
  // Update a To-Do item by ID.
  try {
    const user = req.user; // Assuming the authenticated user is stored in req.user
    const todoId = req.params.id;
    const { title, description, completed } = req.body;

    if (!title) {
        return res.status(400).json({ error: 'The "title" field is required' });
    }

    // Find the To-Do item by ID
    const todo = await TodoModel.findOne({ _id: todoId, user: user._id });

    if (!todo) {
      return res.status(404).json({ error: 'To-Do item not found' });
    }

    // Update the To-Do item
    if (title) todo.title = title;
    if (description) todo.description = description;
    if (completed !== undefined) todo.completed = completed;

    const updatedTodo = await todo.save();

    res.status(200).json(updatedTodo);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Internal server error' });
  }
});

app.delete('/todos/:id', async (req, res) => {
  // Delete a To-Do item by ID.
  try {
    const user = req.user; // Assuming the authenticated user is stored in req.user
    const todoId = req.params.id;

    // Find the To-Do item by ID
    const todo = await TodoModel.findOne({ _id: todoId, user: user._id });

    if (!todo) {
      return res.status(404).json({ error: 'To-Do item not found' });
    }

    // Delete the To-Do item
    await todo.deleteOne();

    res.status(204).send(); // No content (successful deletion)
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Internal server error' });
  }

});

app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
